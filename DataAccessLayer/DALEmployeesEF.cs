using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayer
{
    public class DALEmployeesEF : IDALEmployees
    {
        public void AddEmployee(Employee emp)
        {
            using (var context = new Model.Entities())
            {
                Model.Employee nuevo;
                if (emp.GetType().Name == "FullTimeEmployee")
                {
                    FullTimeEmployee empFullTime = (FullTimeEmployee)emp;
                    nuevo = new Model.FullTimeEmployee()
                    {
                        EmployeeId = empFullTime.Id,
                        Name = empFullTime.Name,
                        StartDate = empFullTime.StartDate,
                        Salary =  empFullTime.Salary
                    };
                    context.Employees.Add(nuevo);
                    context.SaveChanges();
                }
                else
                {
                    PartTimeEmployee empPartTime = (PartTimeEmployee)emp;
                    nuevo = new Model.PartTimeEmployee()
                    {
                        EmployeeId = empPartTime.Id,
                        Name = empPartTime.Name,
                        StartDate = empPartTime.StartDate,
                        HourlyRate = empPartTime.HourlyRate
                    };
                    context.Employees.Add(nuevo);
                    context.SaveChanges();
                }
            }
        }

        public void DeleteEmployee(int id)
        {
            using (var context = new Model.Entities())
            {
                Model.Employee emp = context.Employees.FirstOrDefault(x => x.EmployeeId == id);
                if(emp != null)
                {
                    if(emp.GetType().Name == "FullTimeEmployee")
                    {
                        Model.FullTimeEmployee fullTimeEmp = (Model.FullTimeEmployee)emp;
                        context.Employees.Remove(fullTimeEmp);
                        context.SaveChanges();
                    }
                    else
                    {
                        Model.PartTimeEmployee partTimeEmp = (Model.PartTimeEmployee)emp;
                        context.Employees.Remove(partTimeEmp);
                        context.SaveChanges();
                    }
                }
            }
        }

        public void UpdateEmployee(Employee emp)
        {
            using (var context = new Model.Entities())
            {
                if (emp.GetType().Name == "FullTimeEmployee")
                {
                    FullTimeEmployee empFullTime = (FullTimeEmployee)emp;
                    Model.Employee e = context.Employees.FirstOrDefault(x => x.EmployeeId == emp.Id);
                    if(e != null)
                    {
                        Model.FullTimeEmployee fte = (Model.FullTimeEmployee)e;
                        fte.Name = empFullTime.Name;
                        fte.StartDate = empFullTime.StartDate;
                        fte.Salary = empFullTime.Salary;
                        context.SaveChanges();
                    }
                }
                else
                {
                    PartTimeEmployee empPartTime = (PartTimeEmployee)emp;
                    Model.Employee e = context.Employees.FirstOrDefault(x => x.EmployeeId == emp.Id);
                    if (e != null)
                    {
                        Model.PartTimeEmployee fte = (Model.PartTimeEmployee)e;
                        fte.Name = empPartTime.Name;
                        fte.StartDate = empPartTime.StartDate;
                        fte.HourlyRate = empPartTime.HourlyRate;
                        context.SaveChanges();
                    }
                }
            }
        }

        public List<Employee> GetAllEmployees()
        {
            List<Employee> res = new List<Employee>();
            using (Model.Entities en = new Model.Entities())
            {
                Employee emp;
                en.Employees.ToList().ForEach(x =>
                {
                    if(x.GetType().Name == "FullTimeEmployee")
                    {
                        Model.FullTimeEmployee empFullTime = (Model.FullTimeEmployee)x;
                        emp = new FullTimeEmployee()
                        {
                            Id = empFullTime.EmployeeId,
                            Name = empFullTime.Name,
                            StartDate = empFullTime.StartDate,
                            Salary = empFullTime.Salary
                        };
                    }
                    else
                    {
                        Model.PartTimeEmployee empPartTime = (Model.PartTimeEmployee)x;
                        emp = new PartTimeEmployee()
                        {
                            Id = empPartTime.EmployeeId,
                            Name = empPartTime.Name,
                            StartDate = empPartTime.StartDate,
                            HourlyRate = empPartTime.HourlyRate
                        };
                    }
                    res.Add(emp);
                });
                return res;
            }
        }

        public Employee GetEmployee(int id)
        {
            using (Model.Entities en = new Model.Entities())
            {
                Model.Employee emp= en.Employees.FirstOrDefault(x => x.EmployeeId == id);
                if(emp.GetType().Name == "FullTimeEmployee")
                {
                    Model.FullTimeEmployee fullTimeEmp = (Model.FullTimeEmployee)emp;
                    return new FullTimeEmployee()
                    {
                        Id = fullTimeEmp.EmployeeId,
                        Name = fullTimeEmp.Name,
                        StartDate = fullTimeEmp.StartDate,
                        Salary = fullTimeEmp.Salary
                    };
                }
                else
                {
                    Model.PartTimeEmployee partTimeEmp = (Model.PartTimeEmployee)emp;
                    return new PartTimeEmployee()
                    {
                        Id = partTimeEmp.EmployeeId,
                        Name = partTimeEmp.Name,
                        StartDate = partTimeEmp.StartDate,
                        HourlyRate = partTimeEmp.HourlyRate
                    };
                }
            }
        }
    }
}
