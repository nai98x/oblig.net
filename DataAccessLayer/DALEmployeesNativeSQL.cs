﻿using Shared.Entities;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class DALEmployeesNativeSQL : IDALEmployees
    {
        public void AddEmployee(Employee emp)
        {
            string cs = ConfigurationManager.ConnectionStrings["SQLCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string sql;
                if (emp.GetType().Name == "FullTimeEmployee")
                {
                    FullTimeEmployee fte = (FullTimeEmployee)emp;
                    sql = "INSERT INTO EmployeesTPH (NAME, START_DATE, SALARY, TYPE_EMP) VALUES (@name, @startdate, @salary, @type)";
                    using (SqlCommand com = new SqlCommand(sql, con))
                    {
                        com.Parameters.AddWithValue("@name", fte.Name);
                        com.Parameters.AddWithValue("@startdate", fte.StartDate);
                        com.Parameters.AddWithValue("@salary", fte.Salary);
                        com.Parameters.AddWithValue("@name", 1);
                        _ = com.ExecuteNonQuery();
                    }
                }
                else
                {
                    PartTimeEmployee pte = (PartTimeEmployee)emp;
                    sql = "INSERT INTO EmployeesTPH (NAME, START_DATE, RATE, TYPE_EMP) VALUES (@name, @startdate, @rate, @type)";
                    using (SqlCommand com = new SqlCommand(sql, con))
                    {
                        com.Parameters.AddWithValue("@name", pte.Name);
                        com.Parameters.AddWithValue("@startdate", pte.StartDate);
                        com.Parameters.AddWithValue("@rate", pte.HourlyRate);
                        com.Parameters.AddWithValue("@name", 2);
                        _ = com.ExecuteNonQuery();
                    }
                }
                con.Close();
            }
        }

        public void DeleteEmployee(int id)
        {
            string cs = ConfigurationManager.ConnectionStrings["SQLCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string sql = "DELETE FROM EmployeesTPH WHERE EMP_ID=@Id";
                using (SqlCommand com = new SqlCommand(sql, con))
                {
                    com.Parameters.AddWithValue("@Id", id);
                    _ = com.ExecuteNonQuery();
                }
                con.Close();
            }
        }

        public void UpdateEmployee(Employee emp)
        {
            string cs = ConfigurationManager.ConnectionStrings["SQLCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string sql;
                if (emp.GetType().Name == "FullTimeEmployee")
                {
                    FullTimeEmployee fte = (FullTimeEmployee)emp;
                    sql = "UPDATE EmployeesTPH set NAME = @name, START_DATE = @startdate, SALARY = @salary WHERE EMP_ID = @id";
                    using (SqlCommand com = new SqlCommand(sql, con))
                    {
                        com.Parameters.AddWithValue("@id", fte.Id);
                        com.Parameters.AddWithValue("@name", fte.Name);
                        com.Parameters.AddWithValue("@startdate", fte.StartDate);
                        com.Parameters.AddWithValue("@salary", fte.Salary);
                        _ = com.ExecuteNonQuery();
                    }
                }
                else
                {
                    PartTimeEmployee pte = (PartTimeEmployee)emp;
                    sql = "UPDATE EmployeesTPH set NAME = @name, START_DATE = @startdate, RATE = @rate WHERE EMP_ID = @id";
                    using (SqlCommand com = new SqlCommand(sql, con))
                    {
                        com.Parameters.AddWithValue("@id", pte.Id);
                        com.Parameters.AddWithValue("@name", pte.Name);
                        com.Parameters.AddWithValue("@startdate", pte.StartDate);
                        com.Parameters.AddWithValue("@rate", pte.HourlyRate);
                        _ = com.ExecuteNonQuery();
                    }
                }
                con.Close();
            }
        }

        public List<Employee> GetAllEmployees()
        {
            List<Employee> res = new List<Employee>();
            string cs = ConfigurationManager.ConnectionStrings["SQLCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string sql = "SELECT * FROM EmployeesTPH";
                using (SqlCommand com = new SqlCommand(sql, con))
                {
                    SqlDataReader sqldr = com.ExecuteReader();
                    while (sqldr.Read())
                    {
                        Employee emp;
                        if (sqldr.GetInt32(5) == 1) // Full time
                        {
                            emp = new FullTimeEmployee()
                            {
                                Id = sqldr.GetInt32(0),
                                Name = sqldr.GetString(1),
                                StartDate = sqldr.GetDateTime(2),
                                Salary = sqldr.GetInt32(3)
                            };
                        }
                        else                      // Part time
                        {
                            emp = new PartTimeEmployee()
                            {
                                Id = sqldr.GetInt32(0),
                                Name = sqldr.GetString(1),
                                StartDate = sqldr.GetDateTime(2),
                                HourlyRate = sqldr.GetFloat(4)
                            };
                        }
                        res.Add(emp);
                    }
                }
                con.Close();
            }
            return res;
        }

        public Employee GetEmployee(int id)
        {
            Employee emp = null;
            string cs = ConfigurationManager.ConnectionStrings["SQLCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string sql = "SELECT * FROM EmployeesTPH where EMP_ID = @Id";
                using (SqlCommand com = new SqlCommand(sql, con))
                {
                    com.Parameters.AddWithValue("@Id", id);
                    SqlDataReader sqldr = com.ExecuteReader();
                    while (sqldr.Read())
                    {
                        if (sqldr.GetInt32(5) == 1) // Full time
                        {
                            emp = new FullTimeEmployee()
                            {
                                Id = sqldr.GetInt32(0),
                                Name = sqldr.GetString(1),
                                StartDate = sqldr.GetDateTime(2),
                                Salary = sqldr.GetInt32(3)
                            };
                        }
                        else                       // Part time
                        {
                            emp = new PartTimeEmployee()
                            {
                                Id = sqldr.GetInt32(0),
                                Name = sqldr.GetString(1),
                                StartDate = sqldr.GetDateTime(2),
                                HourlyRate = sqldr.GetFloat(4)
                            };
                        }
                    }
                }
                con.Close();
            }
            return emp;
        }
    }
}
