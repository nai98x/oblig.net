﻿using MongoDB.Bson;
using MongoDB.Driver;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DALEmployeesMongo : IDALEmployees
    {

        public void AddEmployee(Employee emp)
        {
            if(emp != null)
            {
                var client = new MongoClient("mongodb://localhost:27017");
                IMongoDatabase db = client.GetDatabase("ObligatorioNET");
                IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("Employees");
                var document = new BsonDocument();
                if (emp.GetType().Name == "FullTimeEmployee")
                {
                    FullTimeEmployee fte = (FullTimeEmployee)emp;
                    document.Add("ID", fte.Id);
                    document.Add("NAME", fte.Name);
                    document.Add("START_DATE", fte.StartDate);
                    document.Add("TYPE", 1);
                    document.Add("SALARY", fte.Salary);
                }
                else
                {
                    PartTimeEmployee pte = (PartTimeEmployee)emp;
                    document.Add("ID", pte.Id);
                    document.Add("NAME", pte.Name);
                    document.Add("START_DATE", pte.StartDate);
                    document.Add("TYPE", 2);
                    document.Add("RATE", pte.HourlyRate);
                }
                collection.InsertOne(document);
            }
        }

        public void DeleteEmployee(int id)
        {
            var client = new MongoClient("mongodb://localhost:27017");
            IMongoDatabase db = client.GetDatabase("ObligatorioNET");
            IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("Employees");
            var filter = Builders<BsonDocument>.Filter.Eq("ID", id);
            collection.DeleteOne(filter);
        }

        public void UpdateEmployee(Employee emp)
        {
            var client = new MongoClient("mongodb://localhost:27017");
            IMongoDatabase db = client.GetDatabase("ObligatorioNET");
            IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("Employees");
            var filter = Builders<BsonDocument>.Filter.Eq("ID", emp.Id);
            var empleado = collection.Find(filter).Single();
            if (empleado != null)
            {
                int type = empleado.GetValue("TYPE").ToInt32();
                if (type == 1)
                {
                    FullTimeEmployee fte = (FullTimeEmployee)emp;
                    empleado.Set("NAME", fte.Name);
                    empleado.Set("START_DATE", fte.StartDate);
                    empleado.Set("SALARY", fte.Salary);
                    collection.ReplaceOne(filter, empleado);
                }
                else
                {
                    PartTimeEmployee pte = (PartTimeEmployee)emp;
                    empleado.Set("NAME", pte.Name);
                    empleado.Set("START_DATE", pte.StartDate);
                    empleado.Set("RATE", pte.HourlyRate);
                    collection.ReplaceOne(filter, empleado);

                }
            }
        }

        public List<Employee> GetAllEmployees()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            IMongoDatabase db = client.GetDatabase("ObligatorioNET");
            IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("Employees");
            
            List<Employee> ret = new List<Employee>();
            var documents = collection.Find(new BsonDocument()).ToList();
            foreach (BsonDocument doc in documents){
                int id = doc.GetValue("ID").ToInt32();
                string name = doc.GetValue("NAME").AsString;
                DateTime startdate = doc.GetValue("START_DATE").ToLocalTime();
                int type = doc.GetValue("TYPE").ToInt32();
                if (type == 1) // Full Time 
                {
                    int salary = doc.GetValue("SALARY").ToInt32();
                    Employee emp = new FullTimeEmployee()
                    {
                        Id = id,
                        Name = name,
                        StartDate = startdate,
                        Salary = salary
                    };
                    ret.Add(emp);
                }
                else // Part Time
                {
                    int rate = doc.GetValue("RATE").ToInt32();
                    Employee emp = new PartTimeEmployee()
                    {
                        Id = id,
                        Name = name,
                        StartDate = startdate,
                        HourlyRate = rate
                    };
                    ret.Add(emp);
                }
            }
            return ret;
        }

        public Employee GetEmployee(int id)
        {
            var client = new MongoClient("mongodb://localhost:27017");
            IMongoDatabase db = client.GetDatabase("ObligatorioNET");
            IMongoCollection<BsonDocument> collection = db.GetCollection<BsonDocument>("Employees");
            var filter = Builders<BsonDocument>.Filter.Eq("ID", id);
            var empleado = collection.Find(filter).Single();
            if (empleado != null)
            {
                int type = empleado.GetValue("TYPE").ToInt32();
                if (type == 1)
                {
                    return new FullTimeEmployee()
                    {
                        Id = empleado.GetValue("ID").ToInt32(),
                        Name = empleado.GetValue("NAME").AsString,
                        StartDate = empleado.GetValue("START_DATE").ToLocalTime(),
                        Salary = empleado.GetValue("SALARY").ToInt32()
                    };
                }
                else
                {
                    return new PartTimeEmployee()
                    {
                        Id = empleado.GetValue("ID").ToInt32(),
                        Name = empleado.GetValue("NAME").AsString,
                        StartDate = empleado.GetValue("START_DATE").ToLocalTime(),
                        HourlyRate = empleado.GetValue("RATE").ToInt32()
                    };
                }
            }
            return null;
        }
    }
}
