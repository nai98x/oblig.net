﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationLayerWinform
{
    public partial class ListadoPrincipal : Form
    {
        public static CrearEmpleado crearEmpleadoForm;
        public static EditarEmpleado editarEmpleadoForm;
        public static int empSeleccionado;

        public ListadoPrincipal()
        {
            InitializeComponent();
        }

        public void refreshList()
        {
            Controller c = new Controller();
            List<Shared.Entities.Employee> empleados = c.GetAllEmployees();
            List<Shared.Entities.EmployeeWinForm> empleadosList = new List<Shared.Entities.EmployeeWinForm>();
            empleados.ForEach(x =>
            {
                if (x.GetType().Name == "FullTimeEmployee")
                {
                    Shared.Entities.FullTimeEmployee emp = (Shared.Entities.FullTimeEmployee)x;
                    empleadosList.Add(new Shared.Entities.EmployeeWinForm()
                    {
                        Id = emp.Id,
                        Name = emp.Name,
                        StartDate = emp.StartDate,
                        TypeEmp = "Full Time"
                    });
                }
                else
                {
                    Shared.Entities.PartTimeEmployee emp = (Shared.Entities.PartTimeEmployee)x;
                    empleadosList.Add(new Shared.Entities.EmployeeWinForm()
                    {
                        Id = emp.Id,
                        Name = emp.Name,
                        StartDate = emp.StartDate,
                        TypeEmp = "Part Time"
                    });
                }
            });
            dataGridView1.DataSource = empleadosList;
        }

        private void ListadoPrincipal_Load(object sender, EventArgs e)
        {
            buttonActualizar.Hide();
            refreshList();
        }

        private void BotonCrear_Click(object sender, EventArgs e)
        {
            crearEmpleadoForm = new CrearEmpleado();
            crearEmpleadoForm.Show();
            this.Hide();
        }

        private void BotonEditar_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count == 1)
            {
                DataGridViewRow fila = this.dataGridView1.SelectedRows[0];
                empSeleccionado = Int32.Parse(fila.Cells[0].Value.ToString());
                editarEmpleadoForm = new EditarEmpleado();
                editarEmpleadoForm.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Debes seleccionar un empleado.", "Error");
            }
        }

        private void BotonEliminar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                DialogResult res = MessageBox.Show("¿Estas seguro de que quieres eliminar a este empleado?", "Confirmacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (res == DialogResult.OK)
                {
                    DataGridViewRow fila = this.dataGridView1.SelectedRows[0];
                    empSeleccionado = Int32.Parse(fila.Cells[0].Value.ToString());
                    Controller c = new Controller();
                    c.DeleteEmployee(empSeleccionado);
                    this.refreshList();
                }
            }
            else
            {
                MessageBox.Show("Debes seleccionar un empleado.", "Error");
            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void ButtonActualizar_Click(object sender, EventArgs e)
        {
            refreshList();
        }
    }
}
