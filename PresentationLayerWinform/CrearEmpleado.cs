﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationLayerWinform
{
    public partial class CrearEmpleado : Form
    {
        public CrearEmpleado()
        {
            InitializeComponent();
        }

        private void ButtonCrear_Click(object sender, EventArgs e)
        {
            if (textBoxNombre != null && textBoxTipo != null)
            {
                if (radioButtonFull.Checked)
                {
                    Employee emp = new FullTimeEmployee()
                    {
                        Name = textBoxNombre.Text,
                        StartDate = DateTime.Parse(dateTimePicker.Value.ToString("dd/MM/yyyy")),
                        Salary = Int32.Parse(textBoxTipo.Text)
                    };
                    Controller c = new Controller();
                    c.AddEmployee(emp);
                }
                else
                {
                    Employee emp = new PartTimeEmployee()
                    {
                        Name = textBoxNombre.Text,
                        StartDate = DateTime.Parse(dateTimePicker.Value.ToString("dd/MM/yyyy")),
                        HourlyRate = Int32.Parse(textBoxTipo.Text)
                    };
                    Controller c = new Controller();
                    c.AddEmployee(emp);
                }
                this.Close();
                Program.lPrincipal.refreshList();
                Program.lPrincipal.Show();
            }
        }

        private void Button1_Click(object sender, EventArgs e) // Cancelar
        {
            this.Close();
            Program.lPrincipal.refreshList();
            Program.lPrincipal.Show();
        }

        private void CrearEmpleado_Load(object sender, EventArgs e)
        {
            radioButtonFull.Select();
            if(radioButtonFull.Checked == true)
            {
                label5.Text = "Salario:";
            }
            else
            {
                label5.Text = "Por hora:";
            }
            dateTimePicker.Value = DateTime.Today;
        }

        private void RadioButtonFull_CheckedChanged(object sender, EventArgs e)
        {
            label5.Text = "Salario:";
        }

        private void RadioButtonPart_CheckedChanged(object sender, EventArgs e)
        {
            label5.Text = "Por hora:";
        }
    }
}
