﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pruebas
{
    class Principal
    {
        static void Main(string[] args)
        {
            Controller controlador = new Controller();

            string opc;
            Console.WriteLine("Pruebas obligatorio");
            do
            {
                try
                {
                    Console.Write("Opciones:\n   1- Ver empleados\n   2- Buscar empleado\n   3- Agregar empleado\n   4- Modificar empleado\n   5- Eliminar empleado\n");
                    Console.Write("Elija opcion: ");
                    opc = Console.ReadLine();
                    switch (opc)
                    {
                        case "1":
                            Console.Clear();
                            List<Employee> lista= controlador.GetAllEmployees();
                            foreach(Employee e in lista)
                            {
                                if(e.GetType().Name == "FullTimeEmployee")
                                {
                                    FullTimeEmployee fte = (FullTimeEmployee)e;
                                    Console.WriteLine("ID: " + fte.Id.ToString() + " - Tipo: FullTime - Name: " + fte.Name + " - Salary: " + fte.Salary.ToString());
                                }
                                else
                                {
                                    PartTimeEmployee pte = (PartTimeEmployee)e;
                                    Console.WriteLine("ID: " + pte.Id.ToString() + " - Tipo: PartTime - Name: " + pte.Name + " - Hora: " + pte.HourlyRate.ToString());
                                }
                            }
                            break;
                        case "2":
                            Console.Clear();
                            Console.Write("Ingrese ID: ");
                            int idemp =  Int32.Parse(Console.ReadLine());
                            Employee empl = controlador.GetEmployee(idemp);
                            if (empl.GetType().Name == "FullTimeEmployee")
                            {
                                FullTimeEmployee fte = (FullTimeEmployee)empl;
                                Console.WriteLine("ID: " + fte.Id.ToString() + " - Tipo: FullTime - Name: " + fte.Name + " - Salary: " + fte.Salary.ToString());
                            }
                            else
                            {
                                PartTimeEmployee pte = (PartTimeEmployee)empl;
                                Console.WriteLine("ID: " + pte.Id.ToString() + " - Tipo: PartTime - Name: " + pte.Name + " - Hora: " + pte.HourlyRate.ToString());
                            }
                            break;
                        case "3":
                            Console.Clear();

                            break;
                        case "4":
                            Console.Clear();
                            break;
                        case "5":
                            Console.Clear();
                            break;
                        default:
                            Console.WriteLine("Opcion incorrecta.");
                            break;
                    }
                    Console.ReadLine();
                    Console.Clear();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERROR: " + ex.Message);
                }
            } while (!Console.ReadLine().Equals("exit"));
            

            Employee emp = controlador.GetEmployee(3);
            if(emp != null)
            {
                if (emp.GetType().Name == "FullTimeEmployee")
                {
                    FullTimeEmployee emp1 = (FullTimeEmployee)emp;
                    Console.WriteLine("(APP) FullTime: Nombre: " + emp1.Name + " Salario: " + emp1.Salary + "\n");
                }
                else
                {
                    PartTimeEmployee emp2 = (PartTimeEmployee)emp;
                    Console.WriteLine("(APP) PartTime: Nombre: " + emp2.Name + " Por hora: " + emp2.HourlyRate + "\n");
                }
            }
            else
            {
                Console.WriteLine("No anda nada amigo\n");
            }
            _ = Console.ReadLine();
        }
    }
}
