﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using DataAccessLayer;
using BusinessLogicLayer;
using Shared.Entities;

namespace Pruebas
{
    public class Controller
    {
        private IBLEmployees _bl;
        
        public Controller()
        {
            IDALEmployees dal = new DALEmployeesEF();
            _bl = new BLEmployees(dal);
        }

        public void AddEmployee(Employee emp)
        {
            _bl.AddEmployee(emp);
        }

        public void DeleteEmployee(int id)
        {
            _bl.DeleteEmployee(id);
        }

        public void UpdateEmployee(Employee emp)
        {
            _bl.UpdateEmployee(emp);
        }

        public List<Employee> GetAllEmployees()
        {
            return _bl.GetAllEmployees();
        }

        public Employee GetEmployee(int id)
        {
            return _bl.GetEmployee(id);
        }

        public double CalcPartTimeEmployeeSalary(int idEmployee, int hours)
        {
            return _bl.CalcPartTimeEmployeeSalary(idEmployee, hours);
        }
    }
}
